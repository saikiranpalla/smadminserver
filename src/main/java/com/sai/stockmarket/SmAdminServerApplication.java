package com.sai.stockmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableAdminServer
public class SmAdminServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmAdminServerApplication.class, args);
	}

}
